#(How to) Configurar el módulo Blogger
===================================

¿Necesitas mostrar los posts de un blog de **Blogger** en tu App? Entonces estás 
en el lugar adecuado. Con este módulo y el módulo **blogger.single** permitirás 
que los usuarios de tu App puedan ver los últimos posts de tu blog tras unos sencillos pasos. 

El módulo blogger, como todos los demás está **disponible en nuestro market** (y es gratis). Así que sólo hay que seleccionarlo y añadirlo al editor de la App. Como en el resto de módulos, **la configuración es sencilla** pues sólo nos reclama rellenar cuatro campos. Como es natural, podremos cambiarle el nombre al módulo para que al usuario le resulte más descriptivo en el menú. Los campos necesarios son los siguientes:
  
  - **blogId**: ID del blog a mostrar.
 
  - **key**: API key (credencial) de la app.
 
  - **singleurl**: Ruta del módulo que muestra una entrada.
 
  - **maxResults**: Número de entradas a mostrar.
  
## Cómo conseguir los parámetros

### Parámetro blogId: El identificador del blog

Para poder encontrar este parámetro es necesario ir al panel de privado de la cuenta de tu blog en Blogger. Una vez estés en el panel de control sólo es necesario fijarse que en la URL de la página ya se indica el código del blog. Por ejemplo, en la url <https://www.blogger.com/blogger.g?blogID=4178073913348944109#overview> el código del blog es **4178073913348944109** 

### Parámetro key: El identificador de la app

Para poder obtener una credencial hay que hacer dos tareas desde el panel de administración de las APIs de Google: primero hay que crear la credencial y después asignarle permisos para que pueda acceder a la API de Blogger. 

**Habilitar la api de Blogger a la credencial**

![Biblioteca de APIs - Ki_ - https___console.developers.google.com_apis_library](http://kingofapp.es/wp-content/uploads/2015/12/Biblioteca-de-APIs-Ki_-https___console.developers.google.com_apis_library-300x157.png)

Para conseguir el Access Token, deberemos ir a la [consola de desarrolladores de Google](https://console.developers.google.com/). En su página principal podremos ver la opción de "_Habilitar y administrar APIS_". Ahí es donde deberás ir. Una vez en la página correspondiente verás como a la derecha de la pantalla aparecen las opciones específicas para los medios sociales. Selecciona "_Blogger API_" y habilita la API para las credenciales de tu App.


###Parámetro singleurl: La localización de los posts dentro de la App.

Este parámetro necesario indica la dirección interna (dentro de la App) del detalle del post. Al ser una dirección se puede utilizar cualquier módulo de blogger que permita mostrar un post de forma dinámica, como por ejemplo el módulo *bloggersingle*. 

Por ejemplo, si hemos creado un nuevo elemento en la app bajo la dirección */menu/singlepost*, éste tiene que ser el valor del campo *singleurl*. 

###Parámetro maxResults: El número de posts a mostrar

El número indicado en este parámtro indica la longitud de la lista de los posts que se van a mostrar cada vez que un usuario acceda a este punto de menú. Si por ejemplo, este campo valiera 10, se mostrarían los últimos 10 posts añadidos al blog.

## Ejemplo de configuración de los módulos

Este ejemplo muestra cómo configurar los dos módulos *blogger* y *bloggersingle* para que puedan funcionar conjuntamente. Hay que añadir estos campos a la lista de módulos.

    
    "/menu-abcd/blogger-abcd" :{
      "name": "Blogger Example",
      "identifier": "blogger",
      "type": "A",
      "showOn": {
        "menu": true,
        "market": true,
        "dragDrop": true
        },
        "requires": [
            "bloggersingle"
        ],
        "view": "modules/koa.blogger/index.html",
        "files": ["modules/koa.blogger/factory.js", "modules/koa.blogger/directive.js",  "modules/koa.blogger/controller.js", "modules/blogger/style.css"],
        "scope": {
          "blogId": "1234567890123456789",
          "key": "api_key_id",
          "singleurl": "/menu-abcd/blogger-singlepost",
          "maxResults": 10
        }
    },
        "/menu-abcd/blogger-singlepost": {
        "name": "Blogger Post",
      "identifier": "bloggerSingle",
        "type": "A",
        "showOn": {
            "menu": true,
            "market": true,
            "dragDrop": true
        },
        "view": "modules/koa.bloggersingle/index.html",
        "files": ["modules/koa.bloggersingle/controller.js", "modules/koa.bloggersingle/factory.js"],
    "scope": {
          "blogId": "1234567890123456789",
          "key": "api_key_id",
          "postId":"12345678901234567890"
      }
    },
    
En este ejemplo la configuración crearía dos nuevos ítems de menú, uno para la lista de entradas del blog y otro para los detalles de una entrada.

Cada vez que se haga click en el enlace de blogger */menu-abcd/blogger-abcd* se mostrará la lista de entradas del blog con id *1234567890123456789* y, cuando se haga click en una de estas entradas se accederá al detalle de la entrada situado en la url */menu-abcd/blogger-singlepost?post=ID*, siendo ID el identificador único del post dentro de este blog.

En el ítem *bloggersingle* se ha añadido la variable *postId*, que permite también fijar un post por defecto en caso de que no se enviara por parámetro.

 
