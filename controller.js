angular
  .controller('bloggerCtrl', loadFunction);

loadFunction.$inject = ['$scope', '$http', 'structureService', '$location', 'bloggerService'];

function loadFunction($scope, $http, structureService, $location, bloggerService){

  //Register upper level modules
  structureService.registerModule($location,$scope,"blogger");
  var attrs = $scope.blogger.modulescope;
  
  bloggerService.getBlog(attrs.blogId, attrs.key).then(function onGetBlogSuccess(result){
    $scope.blog = result;
  }, function onGetBlogFailure(err){
    $scope.message = "Error retrieving data";
    $scope.err= err;
  });

  bloggerService.getPosts(attrs.blogId, attrs.key, attrs.maxResults).then(function onGetPostsSuccess(data){
    $scope.posts = data;
  }, function onGetPostsFailure(error){
    $scope.message = "Error retrieving data";
    $scope.err = err;
  });

}
