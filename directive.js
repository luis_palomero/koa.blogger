angular.directive('blogPost', [function(){
  return {
    restrict: 'E',
    scope: {
      post: '=',
      link: '='
    },
    link: function(scope, element, attrs){
      var link =  $(element).find('a');
      link.attr('href', '#'+scope.link+'?post='+scope.post.id);
      
    }
  }
}]);