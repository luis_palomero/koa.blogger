'use strict';
angular
    .module('blogger', [])
    .factory(
        'bloggerService',
        function($http, $q) {

          var urls = {
            publicData : 'https://www.googleapis.com/blogger/v3/blogs/:blogId?key=:apiKey',
            posts : 'https://www.googleapis.com/blogger/v3/blogs/:blogId/posts?maxResults=:maxResults&key=:apiKey'
          }
          var getBlog = function(blogId, key) {
            var deferred = $q.defer();
            var url = urls.publicData.replace(':blogId', blogId).replace(':apiKey', key);
            $http.get(url).success(function(data) {
              deferred.resolve(data);
            }).error(function(err) {
              deferred.reject(err);
            });

            return deferred.promise;
          }

          var getPosts = function(blogId, key, _maxResults) {
            var deferred = $q.defer();
            var maxResults = _maxResults || 3;
            var url = urls.posts.replace(':blogId', blogId)
                                .replace(':apiKey', key)
                                .replace(':maxResults', maxResults);
            
            $http.get(url).success(function(data) {
              deferred.resolve(data);
            }).error(function(err) {
              deferred.reject(err);
            });

            return deferred.promise;
          }

          var service = {
            getBlog : getBlog,
            getPosts : getPosts
          }

          return service;
        });
